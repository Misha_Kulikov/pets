package Petss;

/**
 * Класс описывающий котов
 */
public class Cat extends Pets {
    /**
     * Конструктор для котов
     */
    public Cat(String name, int satiety, int fatigue) {
        super(name, satiety, fatigue);
    }
    /**
     *Метод позволяющий котику поиграть
     */

    @Override
    void play() {
        setFatigue(getFatigue() - (int)(10 + Math.random()*8));
        setSatiety(getSatiety() - (int)(7 + Math.random()*13));
        System.out.println("Мяу, я поиграл, Мяу.");
    }
    /**
     *Метод позволяющий котику постпать
     */

    @Override
    void sleep() {
        setFatigue(getFatigue() + (int)(10 + Math.random()*5));
        setSatiety(getSatiety() - (int)(7 + Math.random()*7));
        System.out.println("Немогу ,Мяу, хочу спать");
    }
    /**
     *Метод позволяющий котику кушать
     */
    @Override
    void eat() {
        setFatigue(getFatigue() + (int)(10 + Math.random()*8));
        setSatiety(getSatiety() + (int)(20 + Math.random()*10));
        System.out.println("Дай колбаски ,Мяу");
    }
    public String toString(){
        return getName() + ", устал на " + getFatigue() + "% и сыт на " + getSatiety() + '%';
    }
}