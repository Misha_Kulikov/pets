package Petss;

/**
 * JustMain)
 */


public class Main {

    public static final String FREEDING_FEET = " проголодался(лась) и я его покормил";
    public static final String SLEEPING_MOD = " устал и я его положил(а) спать";
    public static final String TIME_JOY = " хорошо провел(а) время";
    public static final String MOST_HUNGRY = "Самый голодный ";

    public static void main(String[] args) {
        Cat cat = new Cat("Кот", 54, 57);
        Dog dog = new Dog("Сабака", 76, 67);
        Hamster hamster = new Hamster("Хомяк", 75, 42);
        Pets[] animals = new Pets[]{cat, dog, hamster};

        for (int x = 8; x <= 22; x++) {
            System.out.println("Время " + x + ":00");
            for (int y = 0; y < 3; y++) {
                if (animals[y].getSatiety() < 25) {
                    animals[y].eat();
                    System.out.println(animals[y].getName() + FREEDING_FEET);
                } else if (animals[y].getFatigue() < 25) {
                    animals[y].sleep();
                    System.out.println(animals[y].getName() + SLEEPING_MOD);
                } else {
                    animals[y].play();
                    System.out.println(animals[y].getName() + TIME_JOY);
                }


            }
        }
        mostHungry(animals);
    }

    /**
     * Метод вычисляющий самого голодного животного
     */
    public static void mostHungry(Pets[] animals) {
        if (animals[0].getSatiety() < animals[1].getSatiety() && animals[0].getSatiety() < animals[2].getSatiety()) {
            System.out.println(MOST_HUNGRY + animals[0].toString());
        }
        if (animals[1].getSatiety() < animals[0].getSatiety() && animals[1].getSatiety() < animals[2].getSatiety()) {
            System.out.println(MOST_HUNGRY + animals[1].toString());
        }
        if (animals[2].getSatiety() < animals[1].getSatiety() && animals[2].getSatiety() < animals[0].getSatiety()) {
            System.out.println(MOST_HUNGRY + animals[2].toString());
        }
    }
}
