package Petss;

/**
 * Абстрактный Класс для наследования животным
 */
public abstract class Pets {
    /**
     * Поле класс с именем животного
     */
    private String name;
    /**
     * Поле класс с сытостью животного
     */
    private int satiety;
    /**
     * Поле класс с усталостью животного
     */
    private int fatigue;

    /**
     * Конструктор для животных
     */
    public Pets(String name, int satiety, int fatigue) {
        this.name = name;
        this.satiety = satiety;
        this.fatigue = fatigue;
    }

    /**
     * Абстрактный метод игры
     */
    abstract void play();

    /**
     * Абстрактный метод сна
     */
    abstract void sleep();

    /**
     * Абстрактный метод кормления
     */
    abstract void eat();

    void setFatigue(int fatigue) {
        this.fatigue = fatigue;
    }

    int getFatigue() {
        return fatigue;
    }

    void setSatiety(int satiety) {
        this.satiety = satiety;
    }

    int getSatiety() {
        return satiety;
    }

    String getName() {
        return name;
    }

}
