package Petss;

/**
 * Класс описывающий собак
 */
public class Dog extends Pets {
    /**
     * Конструктор для собак
     */
    public Dog(String name, int satiety, int fatigue) {
        super(name, satiety, fatigue);
    }

    /**
     * Метод позволяющий собаке поиграть
     */
    @Override
    void play() {
        setFatigue(getFatigue() - (int) (14 + Math.random() * 5));
        setSatiety(getSatiety() - (int) (5 + Math.random() * 11));
        System.out.println("Гав-Гав , я поиграл!");
    }

    /**
     * Метод позволяющий собаке постпать
     */
    @Override
    void sleep() {
        setFatigue(getFatigue() + (int) (18 + Math.random() * 7));
        setSatiety(getSatiety() - (int) (7 + Math.random() * 4));
        System.out.println("Поспать бы сейчас, УУУууууу");
    }

    /**
     * Метод позволяющий собаке кушать
     */

    @Override
    void eat() {
        setFatigue(getFatigue() + (int) (14 + Math.random() * 8));
        setSatiety(getSatiety() + (int) (22 + Math.random() * 10));
        System.out.println("Гав, дай косточку погрызть");
    }

    public String toString() {
        return getName() + ", устал(а) на " + getFatigue() + "% и сыт(а) на " + getSatiety() + '%';
    }
}
