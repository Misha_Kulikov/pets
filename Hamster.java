package Petss;

/**
 * Класс описывающий хомяков
 */
public class Hamster extends Pets {


    /**
     * Конструктор для хомяков
     */

    public Hamster(String name, int satiety, int fatigue) {
        super(name, satiety, fatigue);
    }

    /**
     * Метод позволяющий хомяку играть
     */
    @Override
    void play() {
        setFatigue(getFatigue() - (int) (8 + Math.random() * 5));
        setSatiety(getSatiety() - (int) (4 + Math.random() * 15));
        System.out.println("Я тут побегал в колисе чутка");
    }

    /**
     * Метод позволяющий хомяку спать
     */
    @Override
    void sleep() {
        setFatigue(getFatigue() + (int) (6 + Math.random() * 7));
        setSatiety(getSatiety() - (int) (7 + Math.random() * 8));
        System.out.println("Давай поспим хозяин?");

    }

    /**
     * Метод позволяющий хомяку кушать
     */
    @Override
    void eat() {
        setFatigue(getFatigue() + (int) (14 + Math.random() * 8));
        setSatiety(getSatiety() + (int) (22 + Math.random() * 15));
        System.out.println("Покушать бы!!");
    }

    public String toString() {
        return getName() + ", устал(а) на " + getFatigue() + "% и сыт(а) на " + getSatiety() + '%';
    }

}
